{-# LANGUAGE DataKinds                #-}

--------------------------------------------------------------------------------
-- |
-- Module      :  HarmTrace.Models.Simple.Main
-- Copyright   :  (c) 2010-2012 Universiteit Utrecht, 2012 University of Oxford
-- License     :  GPL3
--
-- Maintainer  :  bash@cs.uu.nl, jpm@cs.ox.ac.uk
-- Stability   :  experimental
-- Portability :  non-portable
--
-- Summary: The Simple parser
--------------------------------------------------------------------------------

module HarmTrace.Models.Simple.Main ( 
    pSimple, genPiece, getChords
  , module HarmTrace.Models.Simple.Model
  ) where

-- Parser stuff
import Text.ParserCombinators.UU

-- Music stuff
import HarmTrace.Base.MusicRep
import HarmTrace.Models.Collect
import HarmTrace.Models.Parser
import HarmTrace.Models.Generator
import HarmTrace.Models.Simple.Model
import HarmTrace.Models.Simple.Instances ()
import HarmTrace.Models.ChordTokens ( ChordToken )

--------------------------------------------------------------------------------
-- From tokens to structured music pieces
--------------------------------------------------------------------------------

pPieceMaj, pPieceMin :: PMusic [Piece]
pPieceMaj = map Piece <$> amb (parseG :: PMusic [Phrase MajMode])
pPieceMin = map Piece <$> amb (parseG :: PMusic [Phrase MinMode])

pSimple :: Key -> PMusic [Piece]
pSimple (Key _ MajMode) = pPieceMaj
pSimple (Key _ MinMode) = pPieceMin

--------------------------------------------------------------------------------
-- From structured music pieces back to tokens
--------------------------------------------------------------------------------

getChords :: Piece -> [ChordToken]
getChords (Piece phrases) = collectG phrases

--------------------------------------------------------------------------------
-- Generating simple pieces
--------------------------------------------------------------------------------

genPiece :: Key -> Gen Piece
genPiece (Key _ MajMode) = fmap Piece (arbitrary :: Gen [Phrase MajMode])
genPiece (Key _ MinMode) = fmap Piece (arbitrary :: Gen [Phrase MinMode])
